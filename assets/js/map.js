var mapCtrl = {
	mapObject : null,
	tags 	  : {
		fullMapID 		: 'desktop-full-map',
		loaderID  		: 'desktop-map-loader',
		blanketID 		: 'desktop-map-blanket',
		searchInputID 	: 'map-search-string',
		catSelectID   	: 'map-cat-id',
		sortItemClass   : 'map-sort',
		objectsListID   : 'map-objects-list'
	},
	APIInfo : {
		url    : 'execute.php',
		action : 'GetMapObjects'
	},
	filterObject : {
		CatID 		 : null,
		SearchString : null,
		SortID       : 0
	},
	cMarkers 	: [],
	infoWindow 	: null,
	initMap : function() {
		this.mapObject = new google.maps.Map(document.getElementById(this.tags.fullMapID), {
            center: defLatLng,
            gestureHandling: 'greedy',
            zoom: 12,
            styles : [
                { "featureType" : "administrative", "elementType": "geometry", "stylers": [ { "visibility": "off" } ] },
                { "featureType" : "poi", "stylers": [{ "visibility": "off" } ] },
                { "featureType" : "road", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] },
                { "featureType" : "transit", "stylers": [{"visibility": "off" }]}
            ]
        });
        this.initSearchListeners();
        this.initInfoWindow();
        this.loadObjects();
	},
	initSearchListeners(){
		$('#' + this.tags.searchInputID).on('keyup', function(){
			var inputObject = $(this);
				if(inputObject.val().trim() == '') {
					var oldValue = mapCtrl.filterObject.SearchString; 
						mapCtrl.filterObject.SearchString = null;	
						if(oldValue != null) mapCtrl.loadObjects();
						return;
				}
				setTimeout(function(){ 	
					if(inputObject.val().trim() != mapCtrl.filterObject.SearchString) {
						mapCtrl.filterObject.SearchString = inputObject.val().trim();
						mapCtrl.loadObjects();
					} 
				}, 500);
		});

		$('#' + this.tags.catSelectID).on('change', function(){
			var catValue = parseInt($(this).val());
				if(catValue == -1) {
					var oldValue = mapCtrl.filterObject.CatID; 
						mapCtrl.filterObject.CatID = null;
						if(oldValue != null) mapCtrl.loadObjects();					
					return;
				}
				if(catValue != mapCtrl.filterObject.CatID) {
					mapCtrl.filterObject.CatID = catValue;
					mapCtrl.loadObjects(); 
				}
		});

		$('.' + this.tags.sortItemClass).on('click', function(){
			$('.' + mapCtrl.tags.sortItemClass + '[data-sortid='+mapCtrl.filterObject.SortID+']').removeClass('active');
			$(this).addClass('active');
			mapCtrl.filterObject.SortID = $(this).data('sortid');
			mapCtrl.loadObjects();
		});

	},
	loadObjects : function(){
		var filterObject = this.getFilterObject(); 
			this.addLoader();
			filterObject.Action = this.APIInfo.action;
			this.sendRequest(filterObject, this.initObjects);
	},
	getFilterObject(){
		var filterObject = {};
			if(this.filterObject.CatID != null) filterObject.CatID = this.filterObject.CatID;
			if(this.filterObject.SearchString != null) filterObject.SearchString = this.filterObject.SearchString; 
			filterObject.SortID = this.filterObject.SortID;
			return filterObject;
			
	},
	sendRequest :  function(requestData, callback){
		$.post(this.APIInfo.url, requestData).done(function(data){
			callback($.parseJSON(data));
		});
	},
	initObjects : function(objects){
		mapCtrl.removeMarkers();
		mapCtrl.clearObjectItems();
		for(let i in objects) {
			var cMarker = mapCtrl.addMarker(objects[i]);
				google.maps.event.addListener(cMarker['Marker'], 'click', function(){
					mapCtrl.infoWindow.setContent(mapCtrl.getInfoWindowContent(objects[i]));
					mapCtrl.infoWindow.open(mapCtrl.mapObject, this); 
			    	mapCtrl.mapObject.setCenter(mapCtrl.infoWindow.getPosition());
			    });
				mapCtrl.cMarkers.push({'MarkerData' : objects[i], 'MapMarker' : cMarker['Marker']});				
				mapCtrl.addObjectItem(objects[i], objects[i]['ObjectID']);
		}
		mapCtrl.removeLoader();
	},
	onItemClick(objectID){
		for(let cObject in this.cMarkers) {
			if(parseInt(this.cMarkers[cObject].MarkerData['ObjectID']) == parseInt(objectID)) {
				google.maps.event.trigger(this.cMarkers[cObject]['MapMarker'], 'click');
				return;
			}
		}
	},
	clearObjectItems() {
		$('#' + mapCtrl.tags.objectsListID).html('');
	},
	addObjectItem(objectData, objectID){
		var imgSrc  = (objectData['Thumb'] != '') ? 'uploads/' + objectData['Thumb'] : 'assets/res/no_photo.png';
		var logoSrc = (objectData['Logo'] != '') ? 'uploads/' + objectData['Logo'] : 'assets/res/no_photo.png';
			
			$('#' + this.tags.objectsListID).append(''+
				'<div class="object-item" data-id="'+objectID+'" onclick="mapCtrl.onItemClick('+objectID+')">'+
					'<div class="object-content">'+
						'<div class="object-title">'+
							objectData['Title'] + 
						'</div>'+
						'<div class="info">'+
							'<div class="logo">'+
								'<img src="'+logoSrc+'"/>'+
							'</div>'+
							'<div class="info-block">'+
								'<div class="descr">'+objectData['Descr']+'</div>'+
								'<div class="address">'+objectData['Address']+'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
					'<div class="object-image">'+
						'<img src="'+imgSrc+'" />'+
					'</div>'+
				'</div>');
	},
	getInfoWindowContent(objectData) {
		var imgContent = (objectData['Logo'].trim() != '')  ? 	'<div class="infowindow-img">'+
																	'<img src="uploads/'+objectData['Logo']+'" />'+
																'</div>'
															: 	'';
		return '<div class="infowindow-object">'+
					imgContent + 
					'<div class="infowindow-details">'+
						'<a href="index.php?page=object&ObjectID='+objectData['ObjectID']+'" class="infowindow-title">'+objectData['Title']+'</a>'+
						'<div class="infowindow-descr">'+objectData['Descr']+'</div>'+
					'</div>'+
					'<div class="infowindow-action">'+
						'<a href="index.php?page=object&ObjectID='+objectData['ObjectID']+'">'+
							'View Details'
						'</a>'+
					'</div>'+
				'</div>';

	},
	initInfoWindow : function(objectData) { 
		this.infoWindow = new google.maps.InfoWindow(); 
	},
	addMarker : function(markerData) {
		var cMarkerPosition = {lat : parseFloat(markerData['Lat']), lng : parseFloat(markerData['Lon']) }; 
		var mapIcon 		= (markerData['MapIcon'] && markerData['MapIcon'].trim() != '') ? markerData['MapIcon'] : '';
		var markerProperties = {
          	position : cMarkerPosition,
          	map 	 : mapCtrl.mapObject
        };
        if(mapIcon != '') {
        	markerProperties.icon = 'http://www.iguide.ge/uploads/' + mapIcon;
        }
		var cMarker = new google.maps.Marker(markerProperties);
        return {'Marker' : cMarker, 'Position' : cMarkerPosition};
	},
	showMarkers : function() { 
		for(let i in this.cMarkers) this.cMarkers[i].setMap(this.mapObject)
	},
	removeMarkers : function() {
		for(let i in this.cMarkers) { this.cMarkers[i]['MapMarker'].setMap(null); }
	},
	addLoader : function() {
		$('#'+this.tags.blanketID).show();
		$('#'+this.tags.loaderID).show();
	},
	removeLoader : function() {
		$('#'+this.tags.blanketID).hide();
		$('#'+this.tags.loaderID).hide();
	}
}