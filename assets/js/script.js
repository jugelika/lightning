$(document).ready(function(){
	$('.categories-list  .list-group  .list-group-item').on('mouseenter', function(){
		$(this).find('.list-group').slideDown(300);
	});
	$('.categories-list  .list-group  .list-group-item').on('mouseleave', function(){
		$(this).find('.list-group').slideUp(300);
	});
	$('.pr-item').on('mouseenter',function(){
		$(this).find('.hover-block').slideDown(200);
	});
	$('.pr-item').on('mouseleave',function(){
		$(this).find('.hover-block').slideUp(200);
	});
	if($('.zoomable').length > 0) {
		$(".zoomable").elevateZoom();
	}
});